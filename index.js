const path = require('path');
const fs = require('fs');

module.exports = function SkycycleLite(mod) {
	const command = mod.command || mod.require.command
	let aeroInterval = null,
		enabled = true,
		currentAero,
		currentPreset,
		count = 0,
		blendTime,
		cycleTime,
		aero = []

	let config = require('./config.json');
	let presets = require('./presets.json')

	mod.hook('S_SPAWN_ME', 'event', () => {
		mod.setTimeout(() => {
			if(!currentAero) clearAero()
			start(true)
		}, 500)
	})

	function aeroSwitch(aero, forceBlendTime = -1) {
		let sendBlend = blendTime
		if (forceBlendTime >= 0) sendBlend = forceBlendTime
		mod.toClient('S_AERO', 1, {
			enabled: 1,
			blendTime: sendBlend,
			aeroSet: aero
		});
	}

	function timer(forceBlendTime = -1) {
		if (!enabled) return
		if (!config.dungeon && mod.game.me.inDungeon) return
		if (currentPreset.segments) {
				let hourNow = new Date().getUTCHours()
				let minuteNow = new Date().getUTCMinutes()
				let currentSegment = null
				let lastSegment = currentPreset.segments[0]
				for (let seg of currentPreset.segments) {
					if (seg.startHour <= hourNow || seg.startMinute <= minuteNow) currentSegment = seg
					if (seg.startHour>lastSegment.startHour || seg.startMinute>lastSegment.startMinute) lastSegment = seg
				}
				if (currentSegment == null) currentSegment = lastSegment
				currentPreset.aeros = currentSegment.aeros

				if (count > currentPreset.aeros.length) count = 0
				currentAero = currentPreset.aeros[count]
			}
		mod.setTimeout(aeroSwitch, 600, currentAero, forceBlendTime);
	}

	async function start(insta = false, presetMessage = false) {
		if (aero.length == 0) aero = await loadAeros()
		mod.clearInterval(aeroInterval) 
		loadPreset(presetMessage)
		if (!config.dungeon && mod.game.me.inDungeon) return
		if(currentAero && insta) mod.setTimeout(aeroSwitch, 600, currentAero, 0);
		else timer(insta ? 0 : undefined)
		aeroInterval = mod.setInterval(timer, cycleTime)
	}

	function clearAero() {
		mod.toClient('S_START_ACTION_SCRIPT', 3, {
			gameId: mod.game.me.gameId,
			script: 105
		});
	}

	function loadPreset(message) {
		currentPreset = presets[config.activePreset]
		if (message && currentPreset.message) command.message(currentPreset.message)

		blendTime = currentPreset.blendTime
		cycleTime = currentPreset.cycleTime
	}

	command.add(['aero'], (...args) => {
		args[0] = args[0].toLowerCase()
		switch (args[0]) {
			case "on":
				enabled = true;
				command.message('Activated.');
				start()
				break
			case "off":
				enabled = false;
				command.message('Deactivating and reverting');
				clearAero()
				mod.clearInterval(aeroInterval);
				break
			case "restart":
			case "reset":
				enabled = true;
				count = 0
				clearAero()
				start(true)
				break
			case "current":
			case "i":
			case "info":
				command.message(`Current aero is ${currentAero}`)
				command.message(`cycleTime is ${cycleTime}`)
				command.message(`blendTime is ${blendTime}`)
				break
			case 'dungeon':
				config.dungeon = !config.dungeon
				command.message(`${config.dungeon ? 'En' : 'Dis'}abled in dungeons.`)
				break
			default:
				command.message(`Invalid command: ${args.join(' ')}`)
				return
		}
		saveConfig()
	});

	async function loadAeros() {
		let aerolist = []
		let result = await mod.queryData('/ResourceSummary/Package/AeroSet', [], true, false, ['name'])
		for (let obj of result) {
			aerolist.push(obj.attributes.name)
		}
		return aerolist
	}

	function saveConfig() {
		fs.writeFile(path.join(__dirname, 'config.json'), JSON.stringify(config, null, '\t'), err => { });
	}

	this.destructor = () => {
		command.remove(['aero'])
		mod.clearInterval(aeroInterval)
	};
};
