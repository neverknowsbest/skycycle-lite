# Skycycle Lite
A TERA Toolbox module that changes the game's skybox/lighting/effects, giving TERA a more dynamic feel.

This is a fork with basic funcionality, which is shipped with my fork of Toolbox that is being used in our private server. If you want more control over the module, please use the full version [here](https://gitlab.com/neverknowsbest/skycycle).

---
Original idea from [codeagon's cycles](https://github.com/codeagon/cycles)

Fork of [nmods' Aerogasm](https://github.com/nmods/aerogasm)
